package com.tatsum.priority.example.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class UserBean {
    @JsonProperty("name")
    private String name;

    @JsonProperty("pList")
    List<PriorityBean> list = new ArrayList<>();

    public List<PriorityBean> getList() {
        return list;
    }

    public void setList(List<PriorityBean> list) {
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "name='" + name + '\'' +
                ", list=" + list +
                '}';
    }
}
