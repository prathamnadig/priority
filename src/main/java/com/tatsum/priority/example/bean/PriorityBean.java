package com.tatsum.priority.example.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PriorityBean {
    @JsonProperty("priority")
    private String priority;

    @JsonProperty("rating")
    private String rating;

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "PriorityBean{" +
                "priority='" + priority + '\'' +
                ", rating='" + rating + '\'' +
                '}';
    }
}
