package com.tatsum.priority.example.controller;

import com.tatsum.priority.example.service.PriorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController

public class PriorityController {

    @Autowired
    private PriorityService priorityService;

    public PriorityController(@Lazy PriorityService priorityService) {
        this.priorityService = priorityService;
    }


    // Returns a list of all the priority areas from the database
    @RequestMapping(value = "/priorityList", method = RequestMethod.GET)
    public List<String> getListOfPriorities() {
        return priorityService.getListOfPriorities();
    }
}
