package com.tatsum.priority.example.controller;


import com.tatsum.priority.example.bean.UserBean;
import com.tatsum.priority.example.entity.User;
import com.tatsum.priority.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST, produces = {"application/json"}, consumes = "application/json")
    public User savePriorityRatingByUserName(UserBean userBean) {
        return userService.savePriorityRating(userBean);
    }

    @RequestMapping(value = "/userRating", method = RequestMethod.GET)
    public List<User> getAllUserRating() {
        List<User> u = userService.getAllUserRating();
        return u;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public User getUserInfoById(@RequestParam int id) {
        System.out.println(id);
        User u = userService.getUserInfoById(id);
        return u;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public Optional<User> getUserInfoByName(@RequestParam String name) {
        System.out.println(name);
        Optional<User> u = userService.getUserInfoByName(name);
        System.out.println(u);
        return u;
    }
}
