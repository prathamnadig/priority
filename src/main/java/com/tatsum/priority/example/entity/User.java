package com.tatsum.priority.example.entity;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany
    private List<PriorityRating> priorityRatings;

    public List<PriorityRating> getPriorityRatings() {
        return priorityRatings;
    }

    public void setPriorityRatings(List<PriorityRating> priorityRatings) {
        this.priorityRatings = priorityRatings;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name=" + name + '\'' +
                ", priorityRatings=" + priorityRatings +
                '}';
    }
}
