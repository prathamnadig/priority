package com.tatsum.priority.example.entity;

import javax.persistence.*;

@Entity
@Table(name = "PriorityRating")
public class PriorityRating {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "priority")
    private String priority;

    @Column(name = "rating")
    private String rating;

    public String getRating() {
        return rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "PriorityRating{" +
                "id=" + id +
                ", priority='" + priority + '\'' +
                ", rating=" + rating + '\'' +
                '}';
    }
}
