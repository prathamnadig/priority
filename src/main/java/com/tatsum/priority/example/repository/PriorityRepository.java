package com.tatsum.priority.example.repository;

import com.tatsum.priority.example.entity.PriorityRating;
import com.tatsum.priority.example.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriorityRepository extends CrudRepository<PriorityRating, Integer> {

}
