package com.tatsum.priority.example.repository;

import com.tatsum.priority.example.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    public User getUserByName(String name);

}
