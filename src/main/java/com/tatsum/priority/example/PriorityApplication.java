package com.tatsum.priority.example;

import com.tatsum.priority.example.controller.UserController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan(basePackages = {"com.tatsum.priority.example.service","com.tatsum.priority.example.repository","com.tatsum.priority.example.controller"})
public class PriorityApplication {

    public static void main(String[] args) {
        SpringApplication.run(PriorityApplication.class, args);
    }


}
