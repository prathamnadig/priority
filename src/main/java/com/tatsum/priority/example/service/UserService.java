package com.tatsum.priority.example.service;


import com.tatsum.priority.example.bean.PriorityBean;
import com.tatsum.priority.example.bean.UserBean;
import com.tatsum.priority.example.entity.PriorityRating;
import com.tatsum.priority.example.entity.User;
import com.tatsum.priority.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class UserService {

    private final static Logger LOGGER = Logger.getLogger(UserService.class.getName());

    @Autowired
    private UserRepository userRepository;

    //Creates user
    private User createUser(UserBean user) {
        User userEntity = new User();
        List<PriorityRating> priorityRatingList = new ArrayList<>();
        userEntity.setName(user.getName());
        for (PriorityBean priorityBean : user.getList()) {
            PriorityRating priorityRatingEntity = new PriorityRating();
            priorityRatingEntity.setPriority(priorityBean.getPriority());
            priorityRatingEntity.setRating(priorityBean.getRating());
            priorityRatingList.add(priorityRatingEntity);
            userEntity.setPriorityRatings(priorityRatingList);
        }
        return userEntity;
    }

    // Accepts the order of priority along with the satisfaction rating for each area for a user and stores it in the database
    public User savePriorityRating(UserBean user) {
        LOGGER.info("converting bean to entity object");
        User userEntity = createUser(user);

        User userDb = userRepository.save(userEntity);
        System.out.println(userDb);
        return userDb;
    }

    // gets all users and their respective priority and ratings
    public List<User> getAllUserRating() {
        List<User> u = (List<User>) userRepository.findAll();
        return u;
    }

    // gets user and his/her respective priority and rating by id
    public User getUserInfoById(int id) {
        User userDb = userRepository.findById(id).orElse(null);
        return userDb;
    }

    // gets user and his/her respective priority and rating by name //gets user by name
    public Optional<User> getUserInfoByName(String name) {
        Optional<User> u = Optional.ofNullable(userRepository.getUserByName(name));
        return u;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

}

