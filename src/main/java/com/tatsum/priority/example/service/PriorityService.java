package com.tatsum.priority.example.service;

import com.tatsum.priority.example.controller.PriorityController;
import com.tatsum.priority.example.entity.PriorityRating;
import com.tatsum.priority.example.repository.PriorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PriorityService {

    @Autowired
    private PriorityController priorityController;

    @Autowired
    private PriorityRepository priorityRepository;

    //Returns a list of all the priorities from the database
    public List<String> getListOfPriorities() {
        List<PriorityRating> priorityList = (List<PriorityRating>) priorityRepository.findAll();

        List<String> priorityOnly = new ArrayList<>();
        for (PriorityRating p : priorityList) {
            priorityOnly.add(p.getPriority());
        }
        return priorityOnly;
    }
}





