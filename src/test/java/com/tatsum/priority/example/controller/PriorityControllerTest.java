package com.tatsum.priority.example.controller;

import com.tatsum.priority.example.service.PriorityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class PriorityControllerTest {

    PriorityController priorityController = null;
    PriorityService priorityService = null;

    @Before
    public void setUP() {
        priorityService = Mockito.mock(PriorityService.class);
        priorityController = new PriorityController(priorityService);
    }

    @Test
    public void test_getListOfPriorities() throws Exception{

        List<String> list= new ArrayList<>();
        list.add("Health");
        list.add("Wealth");
        list.add("Responsibility");
        list.add("Culture");
        when(priorityService.getListOfPriorities()).thenReturn(list);

       List<String> priorityResponses= priorityController.getListOfPriorities();
        Assert.assertEquals(priorityResponses.size(),list.size());
        Assert.assertEquals(priorityResponses.get(0),list.get(0));
    }
}
