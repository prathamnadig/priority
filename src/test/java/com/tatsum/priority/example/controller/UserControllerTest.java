package com.tatsum.priority.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tatsum.priority.example.bean.PriorityBean;
import com.tatsum.priority.example.bean.UserBean;
import com.tatsum.priority.example.entity.User;
import com.tatsum.priority.example.service.UserService;
import org.junit.Assert;
import org.junit.Before;
//import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.mockito.Mockito.when;

public class UserControllerTest {

    UserService userService = null;

    UserController userController = null;

    @Before
    public void setUP() {
        userService = Mockito.mock(UserService.class);
        userController = new UserController(userService);
    }

    @Test
    public void test_save() throws JsonProcessingException {
        String userJson = "{\n" +
                "    \"userName\":\"pratham\",\n" +
                "    \"pList\":[\n" +
                "        {\n" +
                "            \"priority\":\"health\",\n" +
                "            \"rating\":\"6\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        ObjectMapper mapper = new ObjectMapper();
        UserBean ub = mapper.readValue(userJson, UserBean.class);
        System.out.println(ub);

        User userDb = new User();
        userDb.setName("Pratham");

        List<PriorityBean> priorityBeans= new ArrayList<>();
        PriorityBean priorityBean2=new PriorityBean();
        priorityBean2.setPriority("Culture");
        priorityBean2.setRating("5");

        PriorityBean priorityBean3=new PriorityBean();
        priorityBean3.setPriority("Wealth");
        priorityBean3.setRating("5");

        PriorityBean priorityBean1=new PriorityBean();
        priorityBean1.setPriority("Health");
        priorityBean1.setRating("5");

        priorityBeans.add(priorityBean1);
        priorityBeans.add(priorityBean2);
        priorityBeans.add(priorityBean3);

        when(userService.getUserInfoById(1)).thenReturn(userDb);
        User userResponse = userController.savePriorityRatingByUserName(ub);
        Assert.assertEquals(userResponse.getName(), ub.getName());
        Assert.assertEquals(userResponse.getPriorityRatings().size(),priorityBeans.size());
        Assert.assertTrue(Objects.nonNull(userResponse));

    }
}
